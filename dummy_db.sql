-- MySQL dump 10.13  Distrib 8.0.21, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: dummy
-- ------------------------------------------------------
-- Server version	8.0.21-0ubuntu0.20.04.4

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Logs`
--

DROP TABLE IF EXISTS `Logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Logs` (
  `idLogs` int unsigned NOT NULL AUTO_INCREMENT,
  `action` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `details` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `log_timestamp` datetime NOT NULL,
  PRIMARY KEY (`idLogs`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Logs`
--

LOCK TABLES `Logs` WRITE;
/*!40000 ALTER TABLE `Logs` DISABLE KEYS */;
INSERT INTO `Logs` VALUES (1,'Et consequatur voluptatum est quod tempora vo','Commodi dolorum tenetur voluptatem est commod','1980-02-25 20:35:55'),(2,'Ex eum maxime aut eos.','Odit et nisi cumque eum in facere. Ipsa quis ','2014-04-19 21:00:58'),(3,'Exercitationem repellendus aut ea aut nulla a','Et beatae laborum eos nisi fuga. Aliquid dolo','1992-07-04 15:18:05'),(4,'Sit dicta possimus accusamus deserunt soluta.','Facilis repellat quasi animi modi adipisci al','2008-04-18 14:15:09'),(5,'Voluptatem non pariatur explicabo consequatur','Neque corrupti modi ea odit. Accusamus libero','1994-01-10 18:20:58'),(6,'Quidem rerum iste dicta quidem.','Repudiandae amet asperiores et. Voluptates ve','2006-11-11 09:20:38'),(7,'Voluptatem ad qui in consequatur sunt blandit','Enim velit ipsam et quibusdam aut dolorem lab','2008-07-01 07:34:11'),(8,'Suscipit eos nihil iusto repellendus qui volu','Tenetur et adipisci est dolorem est facilis. ','1987-01-03 12:41:20'),(9,'In ea fuga non quasi minima.','Animi possimus qui vel voluptatum quia et. Re','2019-08-23 13:30:25'),(10,'Voluptatum blanditiis et in eum quia vel.','Exercitationem aspernatur autem illo itaque. ','1974-12-18 14:37:29'),(11,'Dolores dicta totam qui ad aut.','Quia velit rerum delectus nihil. Est perferen','2011-03-21 11:01:34'),(12,'Dolorem quis voluptatum voluptatem dolorem do','Labore doloremque quibusdam sed reprehenderit','2012-02-26 16:17:30'),(13,'Autem expedita maxime aliquid quasi ratione.','Sequi sunt est quia et. Qui hic nemo voluptas','1998-09-17 04:52:28'),(14,'Magni dolor quidem aut non ullam.','Et eius vel atque placeat quis consequuntur p','1987-09-01 10:14:53'),(15,'Illum sint voluptatem voluptas laborum quo do','Hic sequi error ea. Voluptatum tempore aut cu','2003-11-22 07:16:49'),(16,'Iure voluptatem dolore voluptatum similique u','Illo expedita et quidem est harum dolor itaqu','2018-10-12 10:24:35'),(17,'Harum nemo tempora doloribus corrupti in.','Sit hic temporibus quia natus. Aut ipsum repe','1973-09-22 15:06:44'),(18,'Aut commodi quas qui.','Iusto dolorem et perferendis hic et est aut. ','1991-12-26 19:19:28'),(19,'Est molestiae libero illo.','Et aliquam in accusamus consectetur aut. Sed ','1998-09-19 23:00:56'),(20,'Reprehenderit dignissimos dolorum illum earum','Modi repellendus totam reiciendis consequatur','2010-07-22 01:33:53'),(21,'Numquam molestias a aut placeat repellat occa','Voluptas et sit omnis eligendi doloremque exp','1991-08-09 15:00:09'),(22,'Asperiores eligendi quidem alias.','Sed culpa quidem ea totam quo sint laborum. T','1987-12-08 19:01:19'),(23,'Et iusto ut assumenda debitis voluptatem sunt','Omnis eos consequatur sit eum aliquid. Fugiat','1978-11-23 16:01:09'),(24,'At odio nemo eligendi est repellat iure.','Quia voluptas natus ipsa pariatur autem enim ','1980-01-14 12:01:01'),(25,'Aliquid quis hic commodi fugiat.','Aut eveniet non id neque voluptatibus optio i','2020-06-14 01:11:47'),(26,'Recusandae doloribus voluptas nemo nostrum qu','Molestiae veritatis ipsa ex nemo eos. Assumen','2006-06-14 05:55:15'),(27,'Autem consequuntur error mollitia voluptate.','Omnis molestias qui nostrum enim nobis libero','2019-05-12 03:39:47'),(28,'Qui laboriosam magni distinctio qui possimus.','Dolores et cumque quasi rerum rerum consectet','1986-07-08 11:31:25'),(29,'Sed libero possimus nulla quia.','Accusantium placeat nihil beatae dolorem repe','1995-02-23 01:46:22'),(30,'Aut assumenda ab possimus sapiente.','Ut qui asperiores est et eaque. Est possimus ','2004-09-16 16:35:59'),(31,'Ea omnis magnam et dolorem impedit ipsam blan','Quae nesciunt quaerat veniam aperiam et sint.','1982-12-30 19:34:33'),(32,'A laborum corrupti porro eligendi.','Aliquid et nisi cupiditate dolore est. Ea ven','2018-01-23 08:12:25'),(33,'Omnis soluta et rerum eum.','Provident quam et nobis labore. Consectetur d','1998-05-02 12:10:09'),(34,'Eum dolorem qui mollitia accusantium.','Qui ullam nihil in veritatis quam velit adipi','2002-03-08 13:58:16'),(35,'Commodi quis quia cumque nulla et.','Aut voluptates ut incidunt facilis mollitia. ','1974-09-18 10:24:12'),(36,'Necessitatibus pariatur non ad optio quibusda','Quibusdam soluta molestiae architecto occaeca','2001-11-28 02:05:12'),(37,'Quis et iste enim.','Facilis error perferendis quia beatae. Debiti','2016-07-23 10:25:41'),(38,'Fugiat deleniti blanditiis vero perspiciatis.','Quo nobis omnis cum ut. Temporibus voluptate ','2001-09-25 22:33:14'),(39,'Laborum est laudantium ullam excepturi quibus','Et reprehenderit ut hic et inventore nihil ve','2016-05-31 15:52:50'),(40,'Possimus corporis temporibus consequatur dese','In at ab non hic quia. Quae deserunt laudanti','1990-03-30 20:31:05'),(41,'Aut reprehenderit atque et ut totam corrupti ','Eveniet rerum facere quae dolorum dolore. Vit','1985-06-27 21:46:31'),(42,'Magnam soluta harum cumque praesentium facili','Ab distinctio dolor vel dolores maiores. Fugi','2020-09-11 14:44:50'),(43,'Est cum perferendis soluta modi.','Quasi quas ea ab architecto. Et nesciunt prov','1999-08-30 20:47:05'),(44,'Quia itaque amet quisquam nemo quibusdam est.','Similique impedit provident qui exercitatione','1971-02-19 17:20:14'),(45,'In ex quo maiores velit ea magni libero.','Aut harum laudantium consequatur ea. Exceptur','1992-01-09 05:52:55'),(46,'A ratione aliquam vitae.','Architecto tempore minus vel quos. Repudianda','1980-09-29 19:32:52'),(47,'Nulla quae a laboriosam odio quis expedita es','Omnis dicta debitis consequatur repudiandae s','2015-09-06 11:51:16'),(48,'Commodi natus minus rerum quia suscipit.','Quod accusantium et pariatur id officia digni','1972-10-29 18:40:57'),(49,'Autem qui tempora debitis earum.','Reiciendis omnis similique fugiat et dolores ','1971-08-12 15:04:16'),(50,'Sapiente illo ad unde accusantium.','Voluptatibus dolores voluptatem ad hic quia n','1976-07-31 04:06:32'),(51,'Sequi nam expedita fugiat natus exercitatione','Consequuntur officiis sunt illo eum. Fugiat a','1995-04-13 03:03:07'),(52,'Rerum ea sit ratione et error.','Quam accusamus nobis autem eveniet. Ut enim m','2018-07-11 17:19:22'),(53,'Sed dolor sit in dignissimos odio dolores rer','Repudiandae culpa quisquam dolor laborum et. ','2000-07-30 09:52:23'),(54,'Dolores atque sequi dolor qui.','Qui aut quia et et facilis corporis. Molestia','1972-09-13 08:22:10'),(55,'Qui omnis ex numquam voluptas quam voluptatem','Nam rem corrupti rerum quae ipsam hic tempori','2020-02-15 09:39:46'),(56,'Sint sit at vero delectus doloremque ut dolor','Nulla eaque quia tenetur rem culpa et. Sit qu','2015-12-16 06:11:35'),(57,'Provident voluptas et quia qui.','Occaecati voluptate omnis id magni harum sunt','1991-10-24 08:47:23'),(58,'Sit qui eius et facere ut placeat molestiae.','Non suscipit pariatur quidem itaque voluptate','1987-07-17 06:03:38'),(59,'Non aut veniam quas nam.','Dolorem consequuntur corporis qui et voluptas','2010-10-01 11:43:09'),(60,'Pariatur mollitia nihil consequuntur dolor ma','Ducimus autem porro veritatis ipsa qui ut et.','2003-05-04 05:07:29'),(61,'Adipisci non rerum at debitis.','Suscipit est et adipisci et eos officia non a','1974-12-23 19:59:34'),(62,'Earum qui aut nobis et voluptatum nulla.','Id libero cum id et magnam quibusdam. Et reru','2017-05-11 16:21:19'),(63,'Ipsum quis laboriosam a dolore sed iusto simi','Sit inventore sint quia omnis quam aut qui. A','2019-11-11 14:28:56'),(64,'Rerum quis doloremque sit.','Consequatur aut quam animi rerum rerum aut mo','1970-09-28 12:57:55'),(65,'Itaque neque quibusdam et quia corporis conse','Laborum aliquid aut corporis mollitia volupta','1989-10-19 06:06:05'),(66,'Tempore blanditiis est quia dicta nam sit aut','Impedit rem in et minima omnis. Ut aliquam ut','2001-12-02 16:09:30'),(67,'Impedit officiis nostrum quia.','Delectus sed sapiente repellat minus labore. ','1974-12-20 00:18:30'),(68,'Repellendus nisi qui quia tempore laboriosam ','Error eligendi quidem minima ut in architecto','2015-10-27 20:21:37'),(69,'Dolore ullam ut voluptate est fugiat sequi vo','Facere perferendis aut nihil commodi fugiat. ','2001-06-08 07:11:58'),(70,'Et est iste minima et.','Quia sapiente qui aut harum ipsam et. Archite','1989-01-21 22:37:55'),(71,'Voluptatem officiis officiis consequatur dign','Quas vel molestiae eum at et minus nesciunt. ','1993-04-26 23:22:12'),(72,'Voluptatum voluptas ut est ratione reiciendis','Dolor sed occaecati est quo omnis praesentium','1997-05-27 21:08:08'),(73,'Temporibus et nisi numquam aliquid eum minima','Ipsum veniam occaecati est et. Culpa eaque im','2019-11-10 06:50:55'),(74,'Qui ad eaque laboriosam autem et.','Aliquid sit iste quae ratione nemo. Aperiam v','1975-06-24 01:05:54'),(75,'Hic deleniti ipsam vitae ut necessitatibus mi','Molestiae quisquam odio magnam labore odio ip','1996-05-03 14:26:07'),(76,'Omnis voluptatibus dignissimos illum blanditi','Quos est dolorum quo quae. Ex excepturi exerc','1986-10-07 16:00:44'),(77,'Asperiores labore et velit iste sint.','Consequatur sit quo consequuntur dolore est i','2001-02-05 15:20:16'),(78,'Animi sapiente impedit sunt aliquam culpa.','Placeat enim eligendi velit. Rem distinctio i','1997-05-16 17:35:58'),(79,'Eaque cupiditate neque nemo et.','Veritatis hic tempora molestiae laboriosam om','1995-09-28 03:42:48'),(80,'Temporibus modi vel iusto eveniet et possimus','Veritatis consequatur ipsam consequatur conse','1980-04-03 23:41:06'),(81,'Quia aut expedita voluptas quis natus corrupt','Totam atque earum nesciunt ad. Ratione in et ','1974-09-03 20:57:50'),(82,'Earum voluptas culpa eligendi illum.','Voluptatum qui illum enim sed vel. Expedita s','1976-12-02 18:56:01'),(83,'Aut maxime repudiandae autem reiciendis magni','Sit est asperiores corporis deleniti est. Mai','1991-11-27 22:10:27'),(84,'Qui nesciunt tempora a corporis.','Consequatur fugit saepe nobis et. Quia est ac','2009-12-15 11:42:33'),(85,'Voluptatibus at iste sit et vitae debitis dol','Et consequatur enim numquam cumque molestiae ','1990-02-28 20:00:14'),(86,'Accusantium id suscipit et dicta aliquid aut ','Atque repellendus et dolores ut accusantium a','2008-07-13 22:41:34'),(87,'Corrupti et suscipit magni sit occaecati quia','Eius sint voluptas quaerat ullam natus. Qui u','1987-05-30 05:34:56'),(88,'Natus aut voluptatem voluptas et aut.','Quisquam ad voluptatem tempora sed. Natus cor','1980-03-12 04:47:03'),(89,'Beatae ratione itaque ut quia sit soluta.','Accusantium ea quis aut ex necessitatibus off','1983-01-24 03:48:35'),(90,'Repellendus earum ex dicta qui est earum.','Ea facere sunt corrupti voluptas illum et vel','2016-06-11 13:46:22'),(91,'Quia eos veritatis mollitia qui porro.','Tempora autem sint optio quisquam qui qui. Re','1986-11-04 20:48:13'),(92,'Vel omnis impedit non voluptas.','Est nesciunt similique numquam cumque consect','2007-02-11 06:04:54'),(93,'Laboriosam pariatur doloremque fugiat distinc','A commodi assumenda nemo veritatis molestiae.','1978-09-13 22:06:52'),(94,'Quia maxime omnis eum est et.','Eos in consequatur est et ipsa occaecati atqu','2008-01-23 04:49:52'),(95,'Illum harum voluptate ut repellat nisi.','Eos ipsam beatae veritatis quis et illo dolor','1979-03-12 00:45:49'),(96,'Saepe quam delectus cumque quia omnis sequi d','Perferendis deserunt est aliquid omnis nulla ','2013-12-17 16:38:50'),(97,'Aspernatur est ut quia rerum in dolores eum.','Sed molestias asperiores repudiandae est nost','1976-09-09 11:05:43'),(98,'Vel eligendi similique quod.','Incidunt quis repellat aut. Est ut aspernatur','2020-05-25 01:30:41'),(99,'Qui doloremque quod facere ullam labore inven','Id aut ipsum qui autem. Rem ea in excepturi r','1971-10-27 01:39:26'),(100,'Consequatur veritatis voluptatem expedita.','Qui autem distinctio eos velit. Consequuntur ','2018-07-04 00:00:04'),(101,'edit data','huigi3rwjgijnwmmw','2020-10-09 06:33:52');
/*!40000 ALTER TABLE `Logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Municipalities`
--

DROP TABLE IF EXISTS `Municipalities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Municipalities` (
  `idMunicipalities` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`idMunicipalities`),
  UNIQUE KEY `idMunicipalities_UNIQUE` (`idMunicipalities`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Municipalities`
--

LOCK TABLES `Municipalities` WRITE;
/*!40000 ALTER TABLE `Municipalities` DISABLE KEYS */;
INSERT INTO `Municipalities` VALUES (1,'Kertzmannchester'),(2,'New Elza'),(3,'Wendellburgh'),(4,'New Jeremy'),(5,'Champlinmouth'),(6,'Port Lesly'),(7,'East Donnellbury'),(8,'Wilhelminemouth'),(9,'Lake Murphy'),(10,'Josephineland'),(11,'New Isobelmouth'),(12,'Jalynmouth'),(13,'Port Damiontown'),(14,'Goldnerview'),(15,'McDermottbury'),(16,'South Shanel'),(17,'Port Morgan'),(18,'East Newtonhaven'),(19,'West Bridgettestad'),(20,'Lakinchester'),(21,'Uniqueburgh'),(22,'West Rosaliastad'),(23,'North Hollychester'),(24,'North Timmothyhaven'),(25,'East Austin'),(26,'New Santamouth'),(27,'Mayertown'),(28,'Tristinchester'),(29,'North Eltonmouth'),(30,'Kiehnville'),(31,'New Agustin'),(32,'West Declanburgh'),(33,'North Josephinefurt'),(34,'Omahaven'),(35,'South Tevinborough'),(36,'Schneiderland'),(37,'Johnnieborough'),(38,'New Aleenville'),(39,'Camryntown'),(40,'New Giovanna'),(41,'New Savanna'),(42,'West Johnpaul'),(43,'Port Carolanne'),(44,'Nedfurt'),(45,'Angelastad'),(46,'Rosendofort'),(47,'Turnerside'),(48,'South Hipolitofort'),(49,'Lamarton'),(50,'Maybellechester'),(51,'Harberside'),(52,'Port Jazlynchester'),(53,'New Kendall'),(54,'North Jeremyborough'),(55,'Groverville'),(56,'Nickolasview'),(57,'Aishamouth'),(58,'Wildermanburgh'),(59,'West Abbyburgh'),(60,'West Craigstad'),(61,'Klington'),(62,'Labadiehaven'),(63,'East Maureenport'),(64,'Terryfurt'),(65,'Pearlport'),(66,'Adrienneside'),(67,'Benedictmouth'),(68,'Lake Aylachester'),(69,'South Gregg'),(70,'Port Ernestinamouth'),(71,'East Mohammed'),(72,'Johnstonshire'),(73,'Walkertown'),(74,'West Zelmafurt'),(75,'South Sarina'),(76,'West Rosie'),(77,'East Mozelletown'),(78,'Beahanland'),(79,'West Terrellport'),(80,'North Augustushaven'),(81,'Langworthhaven'),(82,'South Leann'),(83,'East Johanna'),(84,'Clintonchester'),(85,'Port Theresabury'),(86,'Rethafurt'),(87,'Christiansenshire'),(88,'New Jordynborough'),(89,'Cartwrightchester'),(90,'North Antonebury'),(91,'East Lewview'),(92,'Magdalenfort'),(93,'Port Shaunfort'),(94,'Port Jaquelineview'),(95,'Deontaebury'),(96,'Kunzehaven'),(97,'Purdyburgh'),(98,'Port Everetteshire'),(99,'DuBuqueshire'),(100,'East Letitialand'),(101,'Glyfada');
/*!40000 ALTER TABLE `Municipalities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Users`
--

DROP TABLE IF EXISTS `Users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Users` (
  `idUsers` int unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `privileges` int unsigned NOT NULL,
  `idMunicipality` int unsigned NOT NULL,
  `password` varchar(455) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`idUsers`),
  UNIQUE KEY `idUsers_UNIQUE` (`idUsers`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  KEY `fk_Users_municipality_idx` (`idMunicipality`),
  CONSTRAINT `fk_Users_municipality` FOREIGN KEY (`idMunicipality`) REFERENCES `Municipalities` (`idMunicipalities`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Users`
--

LOCK TABLES `Users` WRITE;
/*!40000 ALTER TABLE `Users` DISABLE KEYS */;
INSERT INTO `Users` VALUES (1,'armani.witting','Lera Pagac V',4,1,'d43f7bca087dd1e7097008403a09c25c16ea66ca'),(2,'medhurst.francis','Prof. Maudie Cummerata',8,2,'cee323cb209225545f359251959316938789b0e7'),(3,'golda67','Felicita Kshlerin',1,3,'f90d879302b7f204964627c67e5bbdab3a6a91c1'),(4,'kgorczany','Jessika Raynor',9,4,'ffcfba497842f2f39ca132a0fd5e2cd6beb79dec'),(5,'kevin95','Winston Hauck',2,5,'55443ed251f40ece4ecd9c1eab3b76d02854d8b7'),(6,'vhirthe','Tavares Maggio II',6,6,'1c44afe6a0153ee0b8e6e28107adfb57593975ca'),(7,'emuller','Rhea Bernier',8,7,'ea8dfaadb7fda6b71ce8868662b45fd69d103eb0'),(8,'simonis.elliot','Prof. Wayne Murray I',6,8,'17a0428a05e1dbd306b81a104ed07ba68c5a5b0a'),(9,'lind.josh','Asa Hahn',4,9,'faf1ee90a00206673a6f660a1ac07b10053a02cb'),(10,'brooks27','Mellie Weimann II',8,10,'3628ce42871eaac91058fbaa00dbebaaa6af5092'),(11,'april26','Miss Kailey Crooks',0,11,'91ee2c20f5a3b977b1856f03573152f5296ac4c3'),(12,'moshe.kuphal','Miss Shannon Schulist IV',6,12,'88edd22509c9fe7cf9d84fb9eba68b88a3a5f07c'),(13,'lionel.schmitt','Prof. Milford Witting V',9,13,'e81ea922c979700e5a8b9d740c292617cb5b0aa8'),(14,'vada.pacocha','Aileen Jacobi',0,14,'7479d2c89928381a5ab1d7fb80a4cfff38c169ac'),(15,'armani81','Loyce Prohaska',8,15,'462ee448d1ad09311ca61ec7002bce3c662d6fad'),(16,'liana.miller','Earline Koss',9,16,'1f683ad11cabac8ab5f5d4a9e489dcba539c6d7d'),(17,'pgottlieb','Donna Sanford',6,17,'b38bc3e419acbd4eaf4cebdc47283713a93d7971'),(18,'camille.torp','Corbin Corwin',5,18,'416cc07986e06b01006befa5472ad6ff664bb662'),(19,'akohler','Phoebe Carroll',3,19,'04462b16d3314e51b0a2d4aa2d01f540d47c2317'),(20,'eulah.lind','Jaquan Mertz',2,20,'0dc8689a6a659cf4ade2811fd9d578df9eac949c'),(21,'gwendolyn.kuphal','Stacey Fritsch',0,21,'86d153bb3130d4011952de75e33bb7013f7db841'),(22,'elijah.botsford','Pierce Haag',3,22,'a3ce7a8383f6e68816d288e47633356eeea7554c'),(23,'fjenkins','Emilio Carter',3,23,'c620cabbb24b29f63baa521df425cddd069c53d1'),(24,'tyrel53','Mrs. Alicia Ondricka V',5,24,'7968c1bf2aa71ae36332b808b481a10565a04d92'),(25,'andreanne71','Mr. Monty Tillman II',9,25,'f31e8b1c0249fc28800ab298541685a3383ed210'),(26,'makenzie.schultz','Daron Rau',1,26,'a4fa4d0f81164794c1e94ce6eac6ee683856362b'),(27,'raven.luettgen','Estrella Swaniawski',2,27,'974c2d396b3ece16deb49699e8f555434e7cbd5e'),(28,'jkuhn','Velva Grady',8,28,'b021368b98fa20c9ef90d8d46be9151c60e2ccdb'),(29,'barton.buddy','Gladys Strosin',4,29,'ff60525463c4907562e710096ac659c70ae602aa'),(30,'louisa.connelly','Daren Welch',4,30,'42ec5ad8c94d4e7ae65767e344ede7f2af945e0c'),(31,'paris.nitzsche','Tyrel Bruen',3,31,'6302af2bf65d5e96b30543b0109855ca68cee442'),(32,'wraynor','Madaline Yost',6,32,'1ba89d51d36f9cf9090eb436e78dd699f433406d'),(33,'gregoria.thompson','Karianne Zboncak',5,33,'1585fa3dfafbef7706ed29b8c052ec9d7ea7afb9'),(34,'emilie.skiles','Mr. Ruben Jast PhD',4,34,'374caf7ac8e888db141bf6e7ce7d958ce821a8e0'),(35,'samantha82','Prof. Delbert Bechtelar',6,35,'100b5c5dffe66a4533df62685b404e40a646c691'),(36,'mollie.willms','Dr. Sandy Kuphal',1,36,'69e882b52b6cbb9af3f3580b60ac698283d6dc2e'),(37,'dayton.cronin','Benton Gutkowski',3,37,'7ae2cecae7988ba22268c5cacdbeb6b52382a195'),(38,'uhansen','Kelly Murphy',0,38,'6f7a598454b5de43d1ba94ae5ef39b4fea44f763'),(39,'so\'keefe','Willy Gleason',5,39,'8c3c67f7a739ae2771a68555d07daa2373031a9a'),(40,'dexter45','Nelle Walsh',2,40,'f3e28781626fef23d35ed4481548bc34ad0a9478'),(41,'xwaelchi','Katrine Labadie',2,41,'3b47f7caf006b7e2a9843058e930cc094ed8191f'),(42,'ggaylord','Tyrel Kling',6,42,'1a7dfba08a24385eec1bfb4446f66d8e39c26f9a'),(43,'white.colt','Helen Hammes',4,43,'3f829da87f3ecadb7007d8e24d6e8ee72656817d'),(44,'alanna.willms','Stuart Kshlerin',7,44,'5ccaffe001476b097fc256ff9a67c4dd13b8f4a6'),(45,'keara91','Izabella Schmidt MD',3,45,'3ad0d34387b3b620b89021fdc59f60aa50c4b5a9'),(46,'cbartoletti','Rosie Kassulke',2,46,'867cb5391205bb9252733449d287d8537382a1e0'),(47,'lyda.yost','Alessandro Gulgowski',7,47,'ae95110f6ee4f90f799a6be009711c18f05ad2c0'),(48,'henri81','Constantin Bechtelar',2,48,'f3583f69e28834043ef417d916e6da628266d5b6'),(49,'ferry.allen','Nola Dicki',6,49,'986c1935ae440c719a036618dc4db6358b918bb3'),(50,'uwilliamson','Georgette Kuphal V',6,50,'87c21ba2d7087a94943abf11d1448bbd705ff97b'),(51,'cummerata.malachi','Travon Stracke',8,51,'2f51becd878c3a0d6feca8cea989f28c427c9e30'),(52,'smitham.cindy','Lenora Wiegand',3,52,'d76ccd5156f3cd4bc0daa245825a4bf087b6b315'),(53,'leann.schamberger','Johann Willms',9,53,'6af61b28b15a2998fb6055fbe07edbf2fe849e29'),(54,'favian77','Jayden Luettgen',7,54,'f1d0245d0db09c56507b3764a77034515bd23c61'),(55,'chaya03','Jerrold Sporer',4,55,'852f239ed67c27099efe6ba8eb5b42fd6134d1c9'),(56,'felicity98','Sven Schiller',6,56,'a1d45342b4f34eeb9683529840ad7a4ad433966a'),(57,'eldridge85','Leanna Rosenbaum',1,57,'95c718dfed03730a86d9490d643eeb0c1c118191'),(58,'arnulfo00','Rowena Marks',7,58,'45dc9a7939c527152a040c2a163ea7d29a00fcd8'),(59,'zoie59','Jaida Wilderman',6,59,'f59ab3da5dc948dccad88e80f377c0dfd21bc968'),(60,'kilback.kallie','Noemi Tromp',7,60,'2bc07df04eda325aa6217631465d447e8ae74576'),(61,'constance.donnelly','Mrs. Zita Harris',7,61,'428f132b9d8401142e4a617a0ae3e99169aad8b5'),(62,'cfriesen','Dr. Graham Smitham II',8,62,'23e54d8f39d3d058cd520fe2d5c7bd720c20315b'),(63,'phyllis.turcotte','Dr. Nina Huel MD',8,63,'e2754bafb9074ed9858257d5292a4da01678f549'),(64,'freda.schuppe','Maida Nikolaus',8,64,'7744df089bd5ad8fb27847a521f123de72cfe115'),(65,'homenick.linnie','Boris Price',4,65,'22e926c5cc20084199035ff1a9bdff49da690ee1'),(66,'ofunk','Prof. Ebba Breitenberg II',6,66,'b0153c5989f3e3413bdef6760826662eee6db2af'),(67,'travon.sipes','Hillard Fadel',5,67,'9eec7661f0b79711e34dd572ceef7fdc9f09a6af'),(68,'russel.pat','Marvin Haley I',8,68,'dcdc7d77fa331da07f24c755a77f7f007dec5a76'),(69,'russel.jacynthe','Prof. Marjorie Heller',7,69,'fc8d8951cb758b97f40ab77be898fc2caa26dfb7'),(70,'qjaskolski','Dr. Kathryne Stark',4,70,'04b15dcc8b4af409aa88e7835e7a4e46fc62b74a'),(71,'kenneth.brakus','Lenna Feil',8,71,'0c59ebaa0adaab10c40bcb264f80cbb1d2664661'),(72,'evans.quitzon','Naomie Jones',1,72,'4078d7aaa332324d69808183489227216072c15f'),(73,'jkoepp','Sylvia Hessel',1,73,'385c595563c74dbe18344ab6e9586765989d9214'),(74,'marcelina12','Philip Ryan',7,74,'25f8adfdcc81a5a805f70def2c63628f3959b108'),(75,'wilderman.raven','Prof. Rosalyn Treutel',5,75,'e3ea6cab74c86bc4a6ddd8a4d6a65597692d7746'),(76,'quitzon.august','Ruth Jenkins',2,76,'a1eca2ced5e90526da9472f05d2468c9e90d523a'),(77,'kub.lavonne','Aliya Kautzer',0,77,'9aa2085dcd4621b0c069fdcb1989ef386e15a999'),(78,'gleichner.nyasia','Mr. Enrico Halvorson Sr.',0,78,'9b351b76b5361bedc586ece7a2974e67a45631a3'),(79,'qwyman','Joshuah Balistreri MD',8,79,'a148049e30647973fbdc49c8d0d9bfaad53c2400'),(80,'osinski.wendell','Verdie Wilkinson',7,80,'9985257c6bc9e59bc7582eb0b5a8703b600c36bb'),(81,'akassulke','Prof. Syble Walsh',8,81,'1d32049cb377bc6a6dda34ad6b52c7f002feeac5'),(82,'dedric.predovic','Rebekah Hyatt',4,82,'09cecf83c3ebb2b4a5688cde5ac77d28daa7a08c'),(83,'fkirlin','Americo Beer III',6,83,'16cb717fa3420630bc62fdacff490d475305c919'),(84,'maryse05','Mallory Kertzmann',3,84,'04f8db3db999ebb1814f2e42b534d7e423aae931'),(85,'brendan97','Miss Katharina Watsica Jr.',8,85,'938218c9118d85dfc58022f35a84522a23bf0b7c'),(86,'zkuvalis','Rosina Block',0,86,'c02ab0465b9e6f1d89cd87279e44074a1f73af8b'),(87,'mjakubowski','Brent Goldner',5,87,'1dcea09c7be8932f88a71c3dc55eda171142e8e2'),(88,'braden.barrows','Octavia Marquardt',4,88,'2868d696a034d32721787fa9bb4b211dc6759ba1'),(89,'vschuppe','Mrs. Roxanne Conroy MD',9,89,'04c609dc9c4ae3f8532d983858104b0f978cc8f3'),(90,'ritchie.harley','Laverne Nolan',6,90,'fb58e373df082fa3a11fee0b9e2483f0465d0106'),(91,'kaylie42','Caleb Bashirian',5,91,'2ac732ee5a17c281aae571e0b631573cbfdd1487'),(92,'lenore.mccullough','Lydia Schowalter',4,92,'88444de8d373275df90d8db8b7f370dcc0611818'),(93,'bgreen','Dr. Lafayette Farrell',1,93,'03a98fc564b0213b662a0005f5d6bb2b91654255'),(94,'tad51','Mr. Noah McLaughlin',3,94,'b48635982035a88e8791d003ee0e275315078e1f'),(95,'lon35','Edgar Tillman',8,95,'3cfa5632a7035252e125d5faab2a4c5eddfb04f9'),(96,'aemmerich','Ms. Elmira Runte',2,96,'f2b6c084e7208e8e6915a470c88b0407a18bbbe8'),(97,'orlo83','Prof. Reggie Effertz IV',6,97,'83071dfa26f4ae63f765f08d2b936b47353f3c01'),(98,'salvatore.gislason','Ms. Josephine Terry II',3,98,'7d2d83756a5e826ffded166812f283e409ff850e'),(99,'julian.lowe','Eliza Waelchi',8,99,'b53d890ea421933aee0aedee4815af9261a9c1dd'),(100,'vilma.hills','Mr. Casey Spencer II',4,100,'dd349cb96ed3a1577349beef360bdb2fb8662bf3');
/*!40000 ALTER TABLE `Users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Waste_buckets`
--

DROP TABLE IF EXISTS `Waste_buckets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Waste_buckets` (
  `idWaste_buckets` int unsigned NOT NULL AUTO_INCREMENT,
  `waste_type` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `idMunicipality` int unsigned NOT NULL,
  `buckets_num` int unsigned NOT NULL,
  `last_update` datetime NOT NULL,
  PRIMARY KEY (`idWaste_buckets`),
  KEY `fk_Waste_buckets_municipality_idx` (`idMunicipality`),
  CONSTRAINT `fk_Waste_buckets_municipality` FOREIGN KEY (`idMunicipality`) REFERENCES `Municipalities` (`idMunicipalities`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Waste_buckets`
--

LOCK TABLES `Waste_buckets` WRITE;
/*!40000 ALTER TABLE `Waste_buckets` DISABLE KEYS */;
INSERT INTO `Waste_buckets` VALUES (1,' recycling',1,8,'2007-09-03 03:02:41'),(2,'general',2,1,'2001-09-06 08:14:43'),(3,'general',3,6,'1994-01-18 08:34:47'),(4,' recycling',4,1,'2020-04-03 02:35:57'),(5,' aekk',5,1,'2009-07-12 20:07:16'),(6,' gardening',6,1,'2001-07-24 02:12:05'),(7,' recycling',7,6,'2002-10-14 13:10:53'),(8,' biowaste',8,7,'2009-04-30 10:20:38'),(9,' gardening',9,8,'1970-05-16 02:48:35'),(10,' gardening',10,8,'2019-11-21 22:15:19'),(11,' aekk',11,4,'2001-05-26 01:25:03'),(12,' recycling',12,1,'1986-07-27 16:07:12'),(13,' aekk',13,3,'1995-05-12 02:39:38'),(14,' recycling',14,2,'1998-10-20 03:20:02'),(15,'general',15,0,'2007-05-13 03:56:15'),(16,'general',16,3,'1980-10-15 11:27:10'),(17,' biowaste',17,1,'2000-01-01 15:01:58'),(18,' aekk',18,4,'2013-06-06 12:05:43'),(19,' biowaste',19,8,'2001-08-17 12:17:23'),(20,' biowaste',20,3,'2006-07-25 11:20:33'),(21,'general',21,4,'2016-02-10 16:49:19'),(22,' gardening',22,9,'2020-08-07 02:25:10'),(23,'general',23,0,'1999-07-22 00:01:21'),(24,' aekk',24,1,'2007-08-21 11:06:06'),(25,' aekk',25,3,'2005-07-09 23:17:10'),(26,' recycling',26,7,'1986-05-15 19:24:21'),(27,' aekk',27,5,'1999-06-15 10:33:36'),(28,' recycling',28,6,'2020-05-14 16:25:05'),(29,' biowaste',29,5,'1995-07-11 21:00:36'),(30,' aekk',30,8,'1984-04-05 16:10:21'),(31,' gardening',31,0,'1997-10-04 06:18:07'),(32,' recycling',32,1,'1993-12-18 12:13:55'),(33,' recycling',33,0,'1977-11-07 18:31:51'),(34,' biowaste',34,2,'2019-01-24 03:29:28'),(35,' aekk',35,8,'2009-05-27 04:40:42'),(36,'general',36,8,'1991-10-15 07:40:41'),(37,' biowaste',37,4,'1973-07-01 20:52:37'),(38,'general',38,3,'2001-04-20 22:59:32'),(39,'general',39,5,'2018-03-24 20:32:00'),(40,'general',40,9,'1999-12-16 23:34:17'),(41,' recycling',41,2,'1990-12-27 03:37:10'),(42,' gardening',42,4,'1978-09-06 08:50:33'),(43,' biowaste',43,7,'1997-06-20 17:31:53'),(44,'general',44,3,'1999-12-18 15:50:30'),(45,' gardening',45,8,'2015-01-29 17:10:07'),(46,'general',46,3,'1992-12-30 08:07:14'),(47,' recycling',47,1,'2003-11-01 04:59:54'),(48,' aekk',48,1,'1992-12-27 16:00:31'),(49,'general',49,4,'1974-11-09 13:40:19'),(50,' gardening',50,8,'1984-06-16 18:55:18'),(51,' recycling',51,7,'1988-11-01 07:26:20'),(52,' recycling',52,0,'1970-02-13 18:20:10'),(53,' biowaste',53,1,'1996-02-13 18:57:26'),(54,' recycling',54,0,'2012-05-22 16:17:31'),(55,' gardening',55,0,'1977-03-10 01:24:46'),(56,'general',56,3,'1973-02-04 10:00:42'),(57,' biowaste',57,1,'2005-01-11 23:45:32'),(58,' aekk',58,1,'2003-10-07 08:45:12'),(59,'general',59,0,'1970-06-15 19:35:54'),(60,' aekk',60,7,'2004-04-25 19:59:36'),(61,' gardening',61,9,'2009-03-03 10:40:21'),(62,' biowaste',62,5,'1977-09-18 23:35:10'),(63,' biowaste',63,5,'1988-08-12 01:25:25'),(64,' gardening',64,5,'2006-12-24 06:48:29'),(65,' recycling',65,5,'1988-06-11 07:59:52'),(66,' biowaste',66,4,'1988-08-30 03:45:20'),(67,' recycling',67,5,'1984-09-16 14:13:37'),(68,' gardening',68,0,'1997-03-26 17:17:36'),(69,'general',69,8,'2010-02-02 16:06:21'),(70,' gardening',70,5,'2010-04-25 11:37:05'),(71,' gardening',71,0,'2012-10-20 12:53:06'),(72,' aekk',72,9,'1988-11-20 02:22:08'),(73,' biowaste',73,7,'2016-02-21 09:50:36'),(74,' recycling',74,4,'1981-07-12 03:36:29'),(75,' gardening',75,6,'2002-09-04 00:57:20'),(76,' gardening',76,6,'2011-08-20 16:05:22'),(77,' biowaste',77,0,'1971-03-06 13:33:55'),(78,' gardening',78,6,'1997-12-20 06:23:58'),(79,' biowaste',79,8,'2019-06-21 14:58:58'),(80,'general',80,3,'1998-02-07 18:37:26'),(81,' recycling',81,6,'2000-09-30 03:17:45'),(82,'general',82,7,'1976-10-10 14:57:33'),(83,'general',83,7,'1996-12-03 11:01:34'),(84,' recycling',84,5,'2010-03-12 06:20:17'),(85,' gardening',85,2,'1985-12-11 00:51:45'),(86,' gardening',86,3,'1985-07-26 06:45:52'),(87,' gardening',87,8,'2005-08-23 13:52:41'),(88,' recycling',88,4,'1970-08-12 08:12:27'),(89,' recycling',89,0,'2014-05-02 09:49:53'),(90,' biowaste',90,0,'2003-03-22 12:46:50'),(91,'general',91,5,'1980-08-16 14:05:14'),(92,' recycling',92,5,'1970-04-16 06:11:05'),(93,'general',93,6,'2017-03-07 14:14:18'),(94,' gardening',94,4,'2000-06-04 13:44:46'),(95,'general',95,9,'1971-01-11 15:09:41'),(96,' biowaste',96,4,'2007-07-28 05:01:09'),(97,' aekk',97,8,'2008-01-31 11:04:45'),(98,' recycling',98,9,'1992-07-08 16:59:45'),(99,' aekk',99,9,'1981-03-04 10:18:18'),(100,'general',100,0,'1970-10-06 11:53:36');
/*!40000 ALTER TABLE `Waste_buckets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Waste_data`
--

DROP TABLE IF EXISTS `Waste_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Waste_data` (
  `idWaste_data` int unsigned NOT NULL AUTO_INCREMENT,
  `waste_type` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `idMunicipality` int unsigned NOT NULL,
  `waste_kg` decimal(10,2) NOT NULL,
  `data_added` date NOT NULL,
  PRIMARY KEY (`idWaste_data`),
  KEY `fk_Waste_data_municipality_idx` (`idMunicipality`),
  CONSTRAINT `fk_Waste_data_municipality` FOREIGN KEY (`idMunicipality`) REFERENCES `Municipalities` (`idMunicipalities`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Waste_data`
--

LOCK TABLES `Waste_data` WRITE;
/*!40000 ALTER TABLE `Waste_data` DISABLE KEYS */;
INSERT INTO `Waste_data` VALUES (1,' biowaste',1,7515.84,'1975-06-26'),(2,'general',2,8165.80,'1995-01-03'),(3,' gardening',3,4298.77,'1979-06-26'),(4,'general',4,6611.96,'2010-06-10'),(5,' aekk',5,1343.80,'1972-12-26'),(6,' aekk',6,8771.04,'1996-04-19'),(7,' recycling',7,1605.00,'2017-11-18'),(8,' biowaste',8,7700.40,'2007-08-17'),(9,' recycling',9,1130.06,'1990-12-31'),(10,' biowaste',10,4081.15,'1986-09-19'),(11,' biowaste',11,6861.04,'1980-10-31'),(12,' biowaste',12,3684.42,'2013-12-27'),(13,' aekk',13,4257.90,'2017-09-08'),(14,' recycling',14,3487.82,'1996-08-11'),(15,' aekk',15,1120.56,'2016-07-22'),(16,' aekk',16,1084.87,'1970-04-04'),(17,' aekk',17,4137.12,'2014-04-10'),(18,'general',18,3254.30,'1974-03-10'),(19,' recycling',19,2690.87,'2000-12-20'),(20,'general',20,2671.00,'2014-08-02'),(21,' gardening',21,6787.36,'2009-12-10'),(22,' aekk',22,5114.61,'1977-11-30'),(23,' recycling',23,5811.52,'1995-10-11'),(24,' biowaste',24,1251.44,'1999-10-31'),(25,' recycling',25,1886.06,'1993-01-27'),(26,'general',26,5081.64,'1978-12-05'),(27,'general',27,1754.13,'1995-01-27'),(28,' gardening',28,6167.33,'2017-02-06'),(29,' biowaste',29,7312.00,'1976-01-17'),(30,' aekk',30,7312.40,'1988-08-01'),(31,' aekk',31,6492.80,'2018-11-13'),(32,' biowaste',32,4647.11,'2013-06-06'),(33,'general',33,2414.91,'2014-03-26'),(34,'general',34,3775.54,'2016-08-28'),(35,' gardening',35,9802.00,'2001-11-08'),(36,' biowaste',36,8967.88,'2014-09-11'),(37,'general',37,9443.07,'2019-03-12'),(38,' biowaste',38,909.31,'1987-01-14'),(39,' gardening',39,1306.00,'1993-07-09'),(40,' gardening',40,2612.14,'1994-04-15'),(41,' aekk',41,8499.90,'2019-09-15'),(42,' aekk',42,1527.00,'1985-12-17'),(43,'general',43,7709.97,'1983-06-22'),(44,' biowaste',44,4487.03,'2001-07-05'),(45,'general',45,3921.71,'1971-12-29'),(46,' aekk',46,7811.35,'2020-06-04'),(47,'general',47,4886.94,'1985-12-15'),(48,'general',48,7731.78,'1990-12-07'),(49,' recycling',49,7675.49,'1990-03-04'),(50,'general',50,6892.70,'1978-02-27'),(51,'general',51,1255.30,'1982-01-25'),(52,' recycling',52,3978.17,'1982-08-23'),(53,' aekk',53,5901.78,'1987-11-29'),(54,' recycling',54,8632.50,'2016-04-29'),(55,' recycling',55,7042.22,'1984-05-31'),(56,' biowaste',56,7820.00,'1981-04-29'),(57,'general',57,1805.00,'1979-01-31'),(58,' recycling',58,1811.06,'2010-04-12'),(59,' recycling',59,5849.10,'2015-07-25'),(60,' gardening',60,994.52,'2016-06-12'),(61,'general',61,8093.29,'2018-02-13'),(62,' aekk',62,3150.35,'1987-05-12'),(63,' gardening',63,7166.52,'2017-08-29'),(64,' biowaste',64,2482.00,'1985-04-12'),(65,' gardening',65,7350.97,'1990-09-12'),(66,' gardening',66,9811.00,'1992-03-13'),(67,' aekk',67,9846.15,'2009-11-18'),(68,' gardening',68,2282.71,'2013-06-23'),(69,' aekk',69,1839.91,'1971-06-05'),(70,' aekk',70,9478.72,'1974-12-03'),(71,' gardening',71,9266.90,'2017-05-08'),(72,' recycling',72,3350.73,'1976-04-08'),(73,' biowaste',73,2067.20,'1977-04-05'),(74,'general',74,4181.06,'1974-10-11'),(75,' biowaste',75,3646.54,'2000-02-15'),(76,' recycling',76,2401.83,'1989-01-25'),(77,' recycling',77,5739.60,'2018-03-24'),(78,' recycling',78,824.92,'2004-06-24'),(79,' gardening',79,4376.27,'1990-02-19'),(80,' aekk',80,8047.34,'1995-08-19'),(81,'general',81,5621.76,'2009-05-31'),(82,' biowaste',82,2921.96,'2012-01-13'),(83,'general',83,7990.11,'2007-11-19'),(84,' gardening',84,9406.66,'1980-04-27'),(85,'general',85,9380.80,'1970-07-26'),(86,' biowaste',86,1148.86,'1993-08-22'),(87,' biowaste',87,9863.89,'2012-04-27'),(88,' biowaste',88,4338.98,'1997-01-15'),(89,' recycling',89,3989.02,'1992-07-19'),(90,' gardening',90,9863.51,'1986-01-03'),(91,' recycling',91,878.62,'2001-01-28'),(92,' recycling',92,1533.00,'2018-04-01'),(93,' gardening',93,8670.75,'1976-02-11'),(94,'general',94,9732.69,'1983-04-03'),(95,' gardening',95,7525.26,'1983-07-02'),(96,'general',96,7593.02,'1978-09-04'),(97,' biowaste',97,8407.80,'1991-06-22'),(98,' biowaste',98,4386.78,'1970-09-17'),(99,' recycling',99,8609.60,'2015-07-26'),(100,' biowaste',100,7663.97,'2015-09-29');
/*!40000 ALTER TABLE `Waste_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_group` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_group_permissions` (
  `id` int NOT NULL AUTO_INCREMENT,
  `group_id` int NOT NULL,
  `permission_id` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_permission` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add log entry',1,'add_logentry'),(2,'Can change log entry',1,'change_logentry'),(3,'Can delete log entry',1,'delete_logentry'),(4,'Can view log entry',1,'view_logentry'),(5,'Can add permission',2,'add_permission'),(6,'Can change permission',2,'change_permission'),(7,'Can delete permission',2,'delete_permission'),(8,'Can view permission',2,'view_permission'),(9,'Can add group',3,'add_group'),(10,'Can change group',3,'change_group'),(11,'Can delete group',3,'delete_group'),(12,'Can view group',3,'view_group'),(13,'Can add user',4,'add_user'),(14,'Can change user',4,'change_user'),(15,'Can delete user',4,'delete_user'),(16,'Can view user',4,'view_user'),(17,'Can add content type',5,'add_contenttype'),(18,'Can change content type',5,'change_contenttype'),(19,'Can delete content type',5,'delete_contenttype'),(20,'Can view content type',5,'view_contenttype'),(21,'Can add session',6,'add_session'),(22,'Can change session',6,'change_session'),(23,'Can delete session',6,'delete_session'),(24,'Can view session',6,'view_session'),(25,'Can add logs',7,'add_logs'),(26,'Can change logs',7,'change_logs'),(27,'Can delete logs',7,'delete_logs'),(28,'Can view logs',7,'view_logs'),(29,'Can add municipalities',8,'add_municipalities'),(30,'Can change municipalities',8,'change_municipalities'),(31,'Can delete municipalities',8,'delete_municipalities'),(32,'Can view municipalities',8,'view_municipalities'),(33,'Can add users',9,'add_users'),(34,'Can change users',9,'change_users'),(35,'Can delete users',9,'delete_users'),(36,'Can view users',9,'view_users'),(37,'Can add waste buckets',10,'add_wastebuckets'),(38,'Can change waste buckets',10,'change_wastebuckets'),(39,'Can delete waste buckets',10,'delete_wastebuckets'),(40,'Can view waste buckets',10,'view_wastebuckets'),(41,'Can add waste data',11,'add_wastedata'),(42,'Can change waste data',11,'change_wastedata'),(43,'Can delete waste data',11,'delete_wastedata'),(44,'Can view waste data',11,'view_wastedata');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'pbkdf2_sha256$150000$ZdNwEmUp4RJI$8ZV+y+3qPUyPa83FHpzNtDhvQej/cck0YpumbbjqS8s=','2020-10-08 18:45:09.913095',1,'evo','','','',1,1,'2020-10-08 18:44:51.920272');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_user_groups` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `group_id` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `permission_id` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `django_admin_log` (
  `id` int NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int DEFAULT NULL,
  `user_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`),
  CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `django_content_type` (
  `id` int NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'admin','logentry'),(3,'auth','group'),(2,'auth','permission'),(4,'auth','user'),(5,'contenttypes','contenttype'),(7,'main','logs'),(8,'main','municipalities'),(9,'main','users'),(10,'main','wastebuckets'),(11,'main','wastedata'),(6,'sessions','session');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `django_migrations` (
  `id` int NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2020-10-08 14:17:57.891883'),(2,'auth','0001_initial','2020-10-08 14:17:58.063298'),(3,'admin','0001_initial','2020-10-08 14:17:58.518865'),(4,'admin','0002_logentry_remove_auto_add','2020-10-08 14:17:58.666806'),(5,'admin','0003_logentry_add_action_flag_choices','2020-10-08 14:17:58.685082'),(6,'contenttypes','0002_remove_content_type_name','2020-10-08 14:17:58.811362'),(7,'auth','0002_alter_permission_name_max_length','2020-10-08 14:17:58.876219'),(8,'auth','0003_alter_user_email_max_length','2020-10-08 14:17:58.919396'),(9,'auth','0004_alter_user_username_opts','2020-10-08 14:17:58.930951'),(10,'auth','0005_alter_user_last_login_null','2020-10-08 14:17:58.979985'),(11,'auth','0006_require_contenttypes_0002','2020-10-08 14:17:58.984227'),(12,'auth','0007_alter_validators_add_error_messages','2020-10-08 14:17:58.997838'),(13,'auth','0008_alter_user_username_max_length','2020-10-08 14:17:59.065438'),(14,'auth','0009_alter_user_last_name_max_length','2020-10-08 14:17:59.144337'),(15,'auth','0010_alter_group_name_max_length','2020-10-08 14:17:59.177447'),(16,'auth','0011_update_proxy_permissions','2020-10-08 14:17:59.195917'),(17,'sessions','0001_initial','2020-10-08 14:17:59.225932'),(18,'main','0001_initial','2020-10-09 06:19:02.354138');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_expire_date_a5c62663` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('vdlfxqyojdup9jni0xn8ogbl1421v727','YzM5YTI0MDhjOTMzODgwNDZjNTNkNWQ3ZTEwN2EzODY2MDg1NmE2ZDp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIxZDU5NzM3NjA0NzQ5ZjFhMjc5YmY3MjZhNDc0NjkyNmVhMTQ2MjFiIn0=','2020-10-22 18:45:09.959429');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-10-09 22:33:18
