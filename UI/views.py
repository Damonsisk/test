from django.shortcuts import render
import pandas as pd
from datetime import datetime
from django.contrib.auth.decorators import login_required


# input data
# general_waste_16_19 = pd.read_csv('data/gw_16_19.csv')
# general_waste_20 = pd.read_csv('data/gw_20.csv')
# general_waste_20_week = pd.read_csv('data/gw_20_week.csv')
# recycling_waste_16_19 = pd.read_csv("data/rw_16_19.csv")
# recycling_waste_20 = pd.read_csv('data/rw_20.csv')
# bio_waste_20 = pd.read_csv('data/bw_20.csv')
# gardening_waste_20 = pd.read_csv('data/grdw_20.csv')
# aekk_waste_20 = pd.read_csv('data/aw_20.csv')

months = ['Ιανουάριος', 'Φεβρουάριος', 'Μάρτιος', 'Απρίλιος', 'Μάιος', 'Ιούνιος',
          'Ιούλιος', 'Αύγουστος', 'Σεπτέμβριος', 'Οκτώβριος', 'Νοέμβριος', 'Δεκέμβριος']

years = ['2016', '2017', '2018', '2019', '2020']

context = {}


def error_404_view(exception):
    return render(exception, '404.html', {})


# @login_required
def indexPage(request):
    today = datetime.today().strftime('%d-%m-%Y')
    context1 = {
        'today': today,
        'months': months,
    }

    return render(request, 'UI/index.html', context1)


# @login_required
def data_2016(request):
    # context = {'months': months,
    #            'gw_16': general_waste_16_19['Waste KG'].loc[general_waste_16_19['Year'] == int(years[0])].tolist(),
    #            'rw_16': recycling_waste_16_19['Waste KG'].loc[general_waste_16_19['Year'] == int(years[0])].tolist(),
    #            'year_2016': years[0]}

    return render(request, 'UI/2016/2016.html', context)


# @login_required
def data_2017(request):
    # context = {'months': months,
    #            'gw_17': general_waste_16_19['Waste KG'].loc[general_waste_16_19['Year'] == int(years[1])].tolist(),
    #            'rw_17': recycling_waste_16_19['Waste KG'].loc[general_waste_16_19['Year'] == int(years[1])].tolist(),
    #            'year_2017': years[1]}

    return render(request, 'UI/2017.html', context)


# @login_required
def data_2018(request):
    # context = {'months': months,
    #            'gw_18': general_waste_16_19['Waste KG'].loc[general_waste_16_19['Year'] == int(years[2])].tolist(),
    #            'rw_18': recycling_waste_16_19['Waste KG'].loc[general_waste_16_19['Year'] == int(years[2])].tolist(),
    #            'year_2018': years[2]}

    return render(request, 'UI/2018.html', context)


# @login_required
def data_2019(request):
    # context = {'months': months,
    #            'gw_19': general_waste_16_19['Waste KG'].loc[general_waste_16_19['Year'] == int(years[3])].tolist(),
    #            'rw_19': recycling_waste_16_19['Waste KG'].loc[general_waste_16_19['Year'] == int(years[3])].tolist(),
    #            'year_2019': years[3]}

    return render(request, 'UI/2019.html', context)


def general_waste(request):
    # context = {'months': months,
    #            'gw_16': general_waste_16_19['Waste KG'].loc[general_waste_16_19['Year'] == int(years[0])].tolist(),
    #            'gw_17': general_waste_16_19['Waste KG'].loc[general_waste_16_19['Year'] == int(years[1])].tolist(),
    #            'gw_18': general_waste_16_19['Waste KG'].loc[general_waste_16_19['Year'] == int(years[2])].tolist(),
    #            'gw_19': general_waste_16_19['Waste KG'].loc[general_waste_16_19['Year'] == int(years[3])].tolist(),
    #            'gw_20': general_waste_20['Waste KG'].tolist(),
    #            'gw_20_weekly': general_waste_20_week['Waste KG'].tolist(),
    #            'weeks_20': general_waste_20_week['Week'].tolist(),
    #            }

    return (render(request, 'UI/general_waste.html', context))


def add_data(request):
    return render(request, 'UI/test.html', context)


def test(request):
    return render(request, 'UI/test.html', context)
