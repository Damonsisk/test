from django.urls import path
from . import views

urlpatterns = [
    path('', views.indexPage, name='index'),
    path('2016/', views.data_2016, name='data_2016'),
    path('2017/', views.data_2017, name='data_2017'),
    path('2018/', views.data_2018, name='data_2018'),
    path('2019/', views.data_2019, name='data_2019'),
    path('general_waste/', views.general_waste, name='general_waste'),
    path('test/', views.test, name='test'),
    path('add_data/', views.add_data, name='add_data'),
]
