from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout


def profile(request):
    return render(request, 'users/profile.html')


# def logout_attempt(request):
#     logout(request)
#     return render(request, 'users/login.html')
